package com.userservice.UserService.services;

import com.userservice.UserService.dto.UserDTO;
import com.userservice.UserService.entities.UserEntity;
import com.userservice.UserService.repositories.UserRepositiory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Value("${order-service.base-url}")
    private String orderServiceBaseUrl;

    @Value("${order-service.order-url}")
    private String orderServiceOrderUrl;

    @Autowired
    private RestTemplateBuilder restTemplate;

    @Autowired
    private UserRepositiory repositiory;

    public List<UserDTO> getOredrByUserId(Long id)
    {
        List<UserDTO> orders = null;
        try
        {
            orders = restTemplate.build().getForObject(
                    orderServiceBaseUrl.concat(orderServiceOrderUrl).concat("/"+id),
                    List.class
            );
            LOGGER.info("/==============Order by User Id - OK ======/");
        }
        catch (Exception ex)
        {
            LOGGER.warn("/===========OrderBy Id Error : " + ex.getMessage());
        }

        return  orders;
    }


    public List<UserDTO> getAllUsersList()
    {
        List<UserDTO> users = null;
        try
        {
            users = repositiory.findAll() // UserEntity
                    .stream()
                    .map(userEntity -> new UserDTO(
                            userEntity.getId().toString(),
                            userEntity.getName(),
                            userEntity.getAge()
                    )).collect(Collectors.toList());

            LOGGER.info("/==============User List - OK ======/");
        }
        catch (Exception ex)
        {
            LOGGER.warn("/===========User List Error : " + ex.getMessage());
        }

        return users;

    }


    public boolean userSave(Map<String, String> data)
    {
        try
        {
            UserEntity un = new UserEntity();
            un.setName(data.get("name"));
            un.setAge(data.get("age"));
            un.setId(8l);
            repositiory.save(un);
            LOGGER.info("/==============User Save - OK ======/");

            return true;

        }
        catch (Exception ex)
        {
            LOGGER.warn("/===========User Save Error : " + ex.getMessage());
            return  false;
        }
    }


}


