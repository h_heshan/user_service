package com.userservice.UserService.Controlers;

import com.userservice.UserService.dto.UserDTO;
import com.userservice.UserService.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("api/user")
public class UserControler {

    @Autowired
    private UserService userService;

    // localhost:8080/api/user/getAll

    @GetMapping("/getAll")
    public List<UserDTO> getAllUsers()
    {
        return userService.getAllUsersList();
    }

    @GetMapping("/getOrderByUserId/{id}")
    public List<UserDTO> getOrderByUserId(@PathVariable final Long id)
    {
        return userService.getOredrByUserId(id);
    }

    @PostMapping("/save")
    public boolean saveUser(@RequestBody Map<String,String> data)
    {

        return  userService.userSave(data);
    }

}
