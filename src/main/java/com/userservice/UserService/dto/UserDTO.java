package com.userservice.UserService.dto;

public class UserDTO {

    private String age;
    private String id;
    private String name;

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserDTO(String id)
    {

    }

    public UserDTO(String id, String name, String age)
    {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public UserDTO(String name, String age)
    {
        this.name = name;
        this.age = age;
    }

}
